import React from "react";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useSelector } from "react-redux";
import { webRoot } from "../../api/apiRequest";
import { AppState } from "../../store/store";
import ChatMessage from "../../types/chatMessage";

interface Props {
	message: ChatMessage;
	myMessage: boolean;
}

const MediaBubble = (props: Props) => {
	const theme = useSelector((state: AppState) => state.theme.theme);

	return (
		<Row
			style={{ marginBottom: "10px", fontSize: "1.2em" }}
			key={props.message.id}>
			<Col
				xs={12}
				md={
					props.myMessage
						? { offset: 6, span: 6 }
						: { offset: 0, span: 6 }
				}>
				<Card
					bg={
						props.myMessage
							? "info"
							: theme.name === "dark"
							? "light"
							: "dark"
					}
					className={
						props.myMessage && theme.name === "light"
							? "text-white"
							: theme.name === "dark"
							? "text-dark"
							: "text-white"
					}>
					<Card.Body>
						<Card.Subtitle
							className={`${
								props.myMessage ? "text-right" : ""
							} ${
								props.myMessage
									? "text-white"
									: theme.name === "dark"
									? "text-dark"
									: "text-white"
							}`}>
							{props.message.sentTime.format("HH:mm:ss")}
						</Card.Subtitle>
					</Card.Body>
					{props.message.content.includes("webm") ? (
						<video
							controls
							muted
							playsInline={true}
							src={`${webRoot}/images/v1/${props.message.content}`}
						/>
					) : (
						<Card.Img
							src={`${webRoot}/images/v1/${props.message.content}`}
						/>
					)}
				</Card>
			</Col>
		</Row>
	);
};

export default MediaBubble;
