import React from "react";
import Container from "react-bootstrap/Container";
import { useSelector } from "react-redux";
import { AppState } from "../../store/store";
import ChatMessage from "../../types/chatMessage";
import MediaBubble from "./MediaBubble";
import MessageBubble from "./MessageBubble";

interface Props {
	messages: ChatMessage[];
}

const MessageBox = (props: Props) => {
	const myName = useSelector((state: AppState) => state.auth.currentName);

	const myMessage = (message: ChatMessage): boolean => {
		return message.sender === myName;
	};

	return (
		<Container
			style={{
				marginBottom: "70px",
			}}>
			{props.messages.map((message) => {
				if (!message.isImage) {
					return (
						<MessageBubble
							key={message.id}
							myMessage={myMessage(message)}
							message={message}
						/>
					);
				}

				return (
					<MediaBubble
						key={message.id}
						myMessage={myMessage(message)}
						message={message}
					/>
				);
			})}
		</Container>
	);
};

export default MessageBox;
