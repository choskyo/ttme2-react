import React, { ChangeEvent, KeyboardEvent, useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import { useSelector } from "react-redux";
import { apiRequest } from "../../api/apiRequest";
import { MessageTypes, wsConn } from "../../api/websockets";
import { AppState } from "../../store/store";

const InputBar = () => {
	const [message, setMessage] = useState("");
	const [senderId, chatId] = useSelector((state: AppState) => {
		return [state.auth.sessionId, state.chat.id];
	});

	const sendMessage = () => {
		wsConn.send(
			`${MessageTypes.Chat}:${
				sessionStorage.getItem("token") || "asdf"
			}:${message}`
		);

		setMessage("");
	};

	const uploadImage = async (img: File) => {
		const f = new FormData();
		f.append("f", img);

		const h = new Map<string, string>();
		h.set("TTME-Sender", senderId);
		h.set("TTME-Conversation", chatId);

		await apiRequest("images", "v1", "upload", "POST", f, h, false);
	};

	return (
		<InputGroup className="fixed-bottom">
			<InputGroup.Prepend>
				<Button
					title="End conversation."
					size="lg"
					className="rounded-0"
					variant="warning">
					✖
				</Button>
			</InputGroup.Prepend>
			<Form.Control
				size="lg"
				className="rounded-0"
				as="textarea"
				placeholder="Message"
				value={message}
				style={{ resize: "none" }}
				onChange={(e: ChangeEvent<HTMLInputElement>) =>
					setMessage(e.target.value)
				}
				onKeyDown={(e: KeyboardEvent<HTMLInputElement>) => {
					if (e.key === "Enter" && !e.shiftKey && !e.ctrlKey) {
						sendMessage();
					}
				}}
			/>
			<InputGroup.Append>
				<form id="imageUploadForm" action="">
					<input
						type="file"
						id="customFile"
						name="f"
						style={{
							position: "absolute",
							width: "0.1px",
							height: "0.1px",
						}}
						onChange={async (e: ChangeEvent<HTMLInputElement>) => {
							if (e.target.files) {
								await uploadImage(e.target.files[0]);
							}
						}}
					/>
					<Button
						as={"label"}
						htmlFor="customFile"
						className="rounded-0 btn btn-info btn-lg"
						style={{
							height: "100%",
							width: "100%",
							paddingTop: "55%",
						}}>
						＋
					</Button>
				</form>
				<Button
					title="Send message."
					size="lg"
					className="rounded-0"
					variant="success"
					onClick={() => sendMessage()}>
					➤
				</Button>
			</InputGroup.Append>
		</InputGroup>
	);
};

export default InputBar;
