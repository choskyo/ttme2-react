import React, { ChangeEvent } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Navbar from "react-bootstrap/Navbar";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";
import { useDispatch, useSelector } from "react-redux";
import { setAutoscroll } from "../../store/chat/actions";
import { AppState } from "../../store/store";

export const ChatHeaderOptions = () => {
	const chatPartner = useSelector((state: AppState) => state.chat.partner);
	const dispatch = useDispatch();

	return (
		<>
			<Navbar.Text>
				Talking to{" "}
				<OverlayTrigger
					trigger="click"
					placement="bottom"
					overlay={
						<Popover id="partnerPopover">
							<Popover.Content>
								<Button variant="link" className="text-danger">
									Report
								</Button>
							</Popover.Content>
						</Popover>
					}>
					<Navbar.Text
						style={{ borderBottom: "1px solid lightgrey" }}>
						{chatPartner}
					</Navbar.Text>
				</OverlayTrigger>
			</Navbar.Text>
			<Form inline>
				<Form.Check
					style={{ marginLeft: "40px" }}
					className="navbar-text"
					type="checkbox"
					label="Autoscroll"
					onChange={(e: ChangeEvent<HTMLInputElement>) =>
						dispatch(setAutoscroll(e.target.checked))
					}
				/>
			</Form>
		</>
	);
};
