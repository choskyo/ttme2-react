import React, { useEffect } from "react";
import Toast from "react-bootstrap/Toast";
import { useDispatch } from "react-redux";
import { removeError } from "../../store/errors/actions";

interface Props {
	error: { id: string; text: string };
}

const ErrorToast = (props: Props) => {
	const dispatch = useDispatch();

	useEffect(() => {
		setTimeout(() => {
			dispatch(removeError(props.error.id));
		}, 5000);
	}, []);

	return (
		<Toast onClose={() => dispatch(removeError(props.error.id))}>
			<Toast.Header>
				<strong className="mr-auto text-danger">Error</strong>
			</Toast.Header>
			<Toast.Body>{props.error.text}</Toast.Body>
		</Toast>
	);
};

export default ErrorToast;
