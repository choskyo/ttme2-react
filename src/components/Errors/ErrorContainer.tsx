import React from "react";
import { useSelector } from "react-redux";
import { AppState } from "../../store/store";
import ErrorToast from "./ErrorToast";

const ErrorContainer = () => {
	const errors = useSelector((state: AppState) => state.errors.errors);

	return (
		<div
			style={{
				position: "absolute",
				top: 60,
				left: 30,
			}}>
			{errors.map((err) => (
				<ErrorToast key={err.id} error={err} />
			))}
		</div>
	);
};

export default ErrorContainer;
