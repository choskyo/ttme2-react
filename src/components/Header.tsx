import React from "react";
import Button from "react-bootstrap/Button";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useDispatch, useSelector } from "react-redux";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import { AppState } from "../store/store";
import { setTheme } from "../store/theme/actions";
import { ChatHeaderOptions } from "./Chat/ChatHeaderOptions";

const Header = (props: RouteComponentProps) => {
	const theme = useSelector((state: AppState) => state.theme.theme);
	const dispatch = useDispatch();

	return (
		<Navbar
			expand="lg"
			variant={theme.name as "light" | "dark"}
			bg={theme.name as "light" | "dark"}
			fixed="top">
			<Navbar.Brand
				as={Button}
				variant="link"
				onClick={() => props.history.push("/")}>
				話
			</Navbar.Brand>
			{props.history.location.pathname.includes("chat") ? (
				<ChatHeaderOptions />
			) : (
				<></>
			)}
			<Navbar.Toggle />
			<Navbar.Collapse className="justify-content-end">
				<Nav>
					<Button
						as={Link}
						to="/"
						variant="link"
						style={{ color: theme.foreground }}>
						Home
					</Button>
					<Button
						variant="link"
						style={{ color: theme.foreground }}
						onClick={() =>
							theme.name === "light"
								? dispatch(setTheme("dark"))
								: dispatch(setTheme("light"))
						}>
						Toggle Theme
					</Button>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
};

export default withRouter(Header);
