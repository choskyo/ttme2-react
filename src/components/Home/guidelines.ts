const guidelines: string[] = [
	"Rule 1.",
	"Rule 2.",
	"Rule 3.",
	"Report anyone who breaks these rules.",
];

export default guidelines;
