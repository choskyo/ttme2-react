import React from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import guidelines from "./guidelines";

interface State {
	guidelines: string[];
}

export default class Guidelines extends React.Component {
	state = {
		guidelines,
	};

	public render() {
		return (
			<Row className="text-left">
				{this.state.guidelines.map((guideline) => (
					<Col key={guideline} xs={12}>
						¤ {guideline}
					</Col>
				))}
			</Row>
		);
	}
}
