import React from "react";
import Container from "react-bootstrap/Container";
import { useSelector } from "react-redux";
import { Route } from "react-router-dom";
import ErrorContainer from "./components/Errors/ErrorContainer";
import Header from "./components/Header";
import CustomRouter, { history } from "./CustomRouter";
import ChatPage from "./pages/ChatPage";
import HomePage from "./pages/HomePage";
import QueuePage from "./pages/QueuePage";
import { AppState } from "./store/store";

function App() {
	const theme = useSelector((state: AppState) => state.theme.theme);

	return (
		<CustomRouter history={history}>
			<ErrorContainer />
			<Header />
			<br />
			<Container
				style={{
					paddingTop: "60px",
					minHeight: "100%",
					background: theme.background,
					color: theme.foreground,
				}}
				fluid>
				<Route exact path="/" component={HomePage} />
				<Route exact path="/queue" component={QueuePage} />
				<Route exact path="/chat" component={ChatPage} />
			</Container>
		</CustomRouter>
	);
}

export default App;
