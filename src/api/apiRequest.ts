import { history } from "../CustomRouter";
import { addError } from "../store/errors/actions";
import store from "../store/store";

export const webRoot =
	process.env.NODE_ENV === "development"
		? "http://localhost:8080"
		: "http://chat-api.choskyo.net:8080";

export const apiRequest = async (
	service: "images",
	version: string,
	endpoint: string,
	method: "GET" | "POST" | "PUT",
	body: any = undefined,
	extraHeaders: Map<string, string> = new Map(),
	authenticated: boolean = true
) => {
	try {
		const init: RequestInit = {
			headers: {},
			method,
		};
		const headers: any = {};

		extraHeaders.forEach((v, k) => {
			headers[k] = v;
		});

		if (authenticated) {
			headers.Authorization = `Bearer ${
				sessionStorage.getItem("token") || ""
			}`;
		}

		if (method !== "GET" && !headers["Content-Type"]) {
			// headers["Content-Type"] = "application/json";

			if (body) {
				init.body = body;
			}
		}

		init.headers = headers;

		const result = await fetch(
			`${webRoot}/${service}/${version}/${endpoint}`,
			init
		);

		if (!result.ok) {
			const resBody = (await result.json()) as { err: string };

			if (resBody.err) {
				store.dispatch(addError(resBody.err));
			} else {
				store.dispatch(addError(result.statusText));
			}

			if (result.status === 401) {
				history.push("/");
			}
		}

		return result;
	} catch (err) {
		store.dispatch(addError(err.message));

		return {
			ok: false,
		} as Response;
	}
};
