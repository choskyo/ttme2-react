import moment from "moment";
import { history } from "../CustomRouter";
import { setId } from "../store/auth/actions";
import {
	receiveMessage,
	setChatId,
	setPartner,
	setQueueCount,
} from "../store/chat/actions";
import { addError } from "../store/errors/actions";
import store from "../store/store";
import ChatMessage from "../types/chatMessage";
import { webRoot } from "./apiRequest";

export enum MessageTypes {
	Chat = "chat",
	Queue = "queue",
	SetName = "setname",
	Image = "image",
}

export const wsConn = new WebSocket(`ws://${webRoot.split("://")[1]}/ws/v1`);

wsConn.onmessage = (msg: MessageEvent) => {
	const data = msg.data as string;
	const messageType = data.substring(0, data.indexOf(":"));
	const messageContents = data.substring(data.indexOf(":") + 1);

	// console.log(messageContents);

	switch (messageType) {
		case "init": {
			store.dispatch(setId(messageContents));
			break;
		}
		case "queuecount": {
			store.dispatch(setQueueCount(+messageContents));
			break;
		}
		case "chat": {
			try {
				const chatMessage = JSON.parse(messageContents) as ChatMessage;
				chatMessage.sentTime = moment();
				if (!chatMessage.content) {
					throw new Error("invalid message received");
				}
				store.dispatch(receiveMessage(chatMessage));
			} catch (err) {
				store.dispatch(addError(err.message));
			}
			break;
		}
		case "chatStarted": {
			const [chatId, partnerId] = messageContents.split(":");
			store.dispatch(setChatId(chatId));
			store.dispatch(setPartner(partnerId));
			history.push("/chat");
			break;
		}
		case "chatEnded": {
			store.dispatch(addError("Room has been closed by the host."));
			history.push("/lobby");
			break;
		}
	}
};
