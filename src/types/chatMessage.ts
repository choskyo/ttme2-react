import moment from "moment";

export default interface ChatMessage {
	id: string;
	sender: string;
	sentTime: moment.Moment;
	content: string;
	isImage?: boolean;
}
