export const ADD_ERROR = "ADD_ERROR";
export const RMV_ERROR = "RMV_ERROR";

interface AddError {
	type: typeof ADD_ERROR;
	error: string;
}

interface RemoveError {
	type: typeof RMV_ERROR;
	errorId: string;
}

export type ErrorAction = AddError | RemoveError;

export interface ErrorState {
	errors: { id: string; text: string }[];
}
