import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { authReducer } from "./auth/reducers";
import { chatReducer } from "./chat/reducers";
import { errorReducer } from "./errors/reducers";
import { themeReducer } from "./theme/reducers";

const rootReducer = combineReducers({
	errors: errorReducer,
	chat: chatReducer,
	auth: authReducer,
	theme: themeReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
