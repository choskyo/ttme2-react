import ChatMessage from "../../types/chatMessage";

export const SET_PARTNER = "SET_PARTNER";
export const RECEIVE_MESSAGE = "RECEIVEMESSAGE";
export const CLEAR_MESSAGES = "CLEAR_MESSAGES";
export const SET_Q = "SET_QUEUE_COUNT";
export const SET_ID = "SET_CHAT_ID";
export const SET_SCROLL = "SET_SCROLL";

interface SetScroll {
	type: typeof SET_SCROLL;
	scroll: boolean;
}

interface SetChatId {
	type: typeof SET_ID;
	id: string;
}

interface SetQueueCount {
	type: typeof SET_Q;
	len: number;
}

interface SetPartner {
	type: typeof SET_PARTNER;
	partner: string;
}

interface ReceiveMessage {
	type: typeof RECEIVE_MESSAGE;
	message: ChatMessage;
}

interface ClearMessages {
	type: typeof CLEAR_MESSAGES;
}

export type ChatAction =
	| SetQueueCount
	| ReceiveMessage
	| ClearMessages
	| SetPartner
	| SetChatId
	| SetScroll;

export interface ChatState {
	history: ChatMessage[];
	queueCount: number;
	partner: string;
	id: string;
	autoScroll: boolean;
}
