import ChatMessage from "../../types/chatMessage";
import {
	ChatAction,
	ChatState,
	CLEAR_MESSAGES,
	RECEIVE_MESSAGE,
	SET_ID,
	SET_PARTNER,
	SET_Q,
	SET_SCROLL,
} from "./types";

const initialState: ChatState = {
	partner: "",
	history: [] as ChatMessage[],
	queueCount: 0,
	id: "",
	autoScroll: false,
};

export const chatReducer = (
	state = initialState,
	action: ChatAction
): ChatState => {
	switch (action.type) {
		case SET_Q: {
			return { ...state, queueCount: action.len };
		}
		case SET_PARTNER: {
			return { ...state, partner: action.partner };
		}
		case RECEIVE_MESSAGE: {
			return {
				...state,
				history: [...state.history, action.message],
			};
		}
		case CLEAR_MESSAGES: {
			return {
				...state,
				history: [],
			};
		}
		case SET_ID: {
			return { ...state, id: action.id };
		}
		case SET_SCROLL: {
			return { ...state, autoScroll: action.scroll };
		}
		default: {
			return state;
		}
	}
};
