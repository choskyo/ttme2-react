import ChatMessage from "../../types/chatMessage";
import {
	ChatAction,
	CLEAR_MESSAGES,
	RECEIVE_MESSAGE,
	SET_ID,
	SET_PARTNER,
	SET_Q,
	SET_SCROLL,
} from "./types";

export const setAutoscroll = (scroll: boolean): ChatAction => {
	return {
		type: SET_SCROLL,
		scroll,
	};
};

export const setChatId = (id: string): ChatAction => {
	return {
		type: SET_ID,
		id,
	};
};

export const setQueueCount = (len: number): ChatAction => {
	return {
		type: SET_Q,
		len,
	};
};

export const receiveMessage = (message: ChatMessage): ChatAction => {
	return {
		type: RECEIVE_MESSAGE,
		message,
	};
};

export const clearMessages = (): ChatAction => {
	return {
		type: CLEAR_MESSAGES,
	};
};

export const setPartner = (name: string): ChatAction => {
	return {
		type: SET_PARTNER,
		partner: name,
	};
};
