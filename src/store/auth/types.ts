export const SETNAME = "SETNAME";
export const SET_ID = "SET_ID";

interface SetName {
	type: typeof SETNAME;
	name: string;
}

interface SetId {
	type: typeof SET_ID;
	id: string;
}

export type AuthAction = SetName | SetId;

export interface AuthState {
	currentName: string;
	sessionId: string;
}
