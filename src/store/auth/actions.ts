import { AuthAction, SETNAME, SET_ID } from "./types";

export const setId = (id: string): AuthAction => {
	return {
		type: SET_ID,
		id,
	};
};

export const setName = (newName: string): AuthAction => {
	return {
		type: SETNAME,
		name: newName,
	};
};
