import { AuthAction, AuthState, SETNAME, SET_ID } from "./types";

const initialState: AuthState = {
	currentName: "",
	sessionId: "",
};

export const authReducer = (
	state = initialState,
	action: AuthAction
): AuthState => {
	switch (action.type) {
		case SETNAME: {
			return { ...state, currentName: action.name };
		}
		case SET_ID: {
			return { ...state, sessionId: action.id };
		}
		default: {
			return state;
		}
	}
};
