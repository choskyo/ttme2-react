export const SET_THEME = "SET_THEME";

export interface Theme {
	name: string;
	background: string;
	foreground: string;
}

export const DarkTheme: Theme = {
	name: "dark",
	background: "#343a40",
	foreground: "#f8f9fa",
};

export const LightTheme: Theme = {
	name: "light",
	background: "#f8f9fa",
	foreground: "#343a40",
};

interface SetTheme {
	type: typeof SET_THEME;
	theme: "light" | "dark";
}

export type ThemeAction = SetTheme;

export interface ThemeState {
	theme: Theme;
}
