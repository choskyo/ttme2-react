import {
	DarkTheme,
	LightTheme,
	SET_THEME,
	ThemeAction,
	ThemeState,
} from "./types";

const initialState: ThemeState = {
	theme: localStorage.getItem("theme") === "dark" ? DarkTheme : LightTheme,
};

export const themeReducer = (
	state = initialState,
	action: ThemeAction
): ThemeState => {
	switch (action.type) {
		case SET_THEME: {
			const newTheme = action.theme === "light" ? LightTheme : DarkTheme;

			localStorage.setItem("theme", newTheme.name);

			return {
				...state,
				theme: newTheme,
			};
		}
		default: {
			return state;
		}
	}
};
