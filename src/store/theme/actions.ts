import { SET_THEME, ThemeAction } from "./types";

export const setTheme = (theme: "light" | "dark"): ThemeAction => {
	return {
		type: SET_THEME,
		theme,
	};
};
