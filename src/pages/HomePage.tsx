import React, { ChangeEvent, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import { useDispatch } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import Guidelines from "../components/Home/Guidelines";
import { setName } from "../store/auth/actions";

const HomePage = (props: RouteComponentProps) => {
	const dispatch = useDispatch();
	const [input, setInput] = useState(
		"User " + Math.floor(Math.random() * 10000)
	);

	const queue = () => {
		dispatch(setName(input));

		props.history.push("/queue");
	};

	return (
		<Col className="text-center" xs={12}>
			<h1 style={{ marginBottom: "40px" }}>
				Enter a name to get started
			</h1>
			<Col
				xs={12}
				sm={{ offset: 4, span: 4 }}
				style={{ marginBottom: "40px" }}>
				<Form.Control
					style={{ marginBottom: "20px" }}
					placeholder={input}
					onChange={(e: ChangeEvent<HTMLInputElement>) =>
						setInput(e.target.value)
					}
				/>
				<Button onClick={() => queue()} variant="primary">
					Start talking
				</Button>
			</Col>
			<Col xs={12} sm={{ offset: 2, span: 8 }}>
				<Guidelines />
			</Col>
		</Col>
	);
};

export default withRouter(HomePage as any);
