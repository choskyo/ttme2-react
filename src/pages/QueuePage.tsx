import React, { useEffect } from "react";
import Container from "react-bootstrap/Container";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { wsConn } from "../api/websockets";
import { AppState } from "../store/store";

const QueuePage = () => {
	const history = useHistory();
	const [queueCount, currentName] = useSelector((state: AppState) => {
		return [state.chat.queueCount, state.auth.currentName];
	});

	useEffect(() => {
		if (!currentName) {
			return history.push("/");
		}

		wsConn.send("queue:asdf:" + currentName);

		return () => {
			if (currentName) {
				wsConn.send("unqueue:asdf:");
			}
		};
	}, []);

	return (
		<Container className="text-center">
			<h3>{queueCount} in queue. </h3>
		</Container>
	);
};

export default QueuePage;
