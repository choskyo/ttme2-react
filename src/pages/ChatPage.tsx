import React, { useEffect } from "react";
import Row from "react-bootstrap/Row";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import InputBar from "../components/Chat/InputBar";
import MessageBox from "../components/Chat/MessageBox";
import { AppState } from "../store/store";

const ChatPage = () => {
	const [chatId, messages, autoScroll] = useSelector((state: AppState) => {
		return [state.chat.id, state.chat.history, state.chat.autoScroll];
	});
	const history = useHistory();

	if (!chatId) {
		history.push("/");
	}

	useEffect(() => {
		if (autoScroll) {
			setTimeout(() => {
				window.scrollTo(0, document.body.scrollHeight);
			}, 200);
		}
	}, [messages, autoScroll]);

	return (
		<Row>
			<div style={{ marginBottom: "15px" }} className="w-100"></div>
			<MessageBox messages={messages} />
			<div style={{ marginBottom: "60px" }} className="w-100"></div>
			<InputBar />
		</Row>
	);
};

export default ChatPage;
